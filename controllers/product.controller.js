import Product from '../models/product.models.js';

export const getProducts = async(req, res)=>{
    try{
        const product = await Product.findAll();
        res.send(product)
    }catch(err){
        console.log(err)
    }
}

export const getProductById = async (req, res)=>{
    try{
        const product = await Product.findAll(
            {
                where:{
                    id:req.params.id
                }
            });
            if(product[0]===undefined){
                res.status(404).json({msg:'data tidak ditemukan'})
            }
            res.json(product[0])
    }catch(err){
        console.log(err)
    }
}

export const createProduct = async (req, res)=>{
    try{
        await Product.create(req.body)
        res.json({msg:'product created'})
    }catch(err){
        console.log(err)
    }
}

export const updateProduct = async (req, res)=>{
    try{
        await Product.update(req.body, {
            where:{
                id:req.params.id
            }
        });
        res.json({msg:"product updated"})
    }catch(err){
        console.log(err)
    }
}

export const deleteProduct = async(req, res)=>{
    try{
        await Product.destroy({
            where:{
                id:req.params.id
            }
        });
        res.json({msg:`product id deleted${req.params.id}`});
    }catch(err){
        console.log(err);
    }
}