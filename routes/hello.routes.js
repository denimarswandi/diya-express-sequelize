import express from 'express'

const routerHello = express.Router()

routerHello.get('/', (req, res)=>{
    res.json({msg:'ok'})
})

export default routerHello