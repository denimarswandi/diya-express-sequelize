import express from 'express';

const router = express.Router()

import routerProduct from './product.routes.js'
import routerHello from './hello.routes.js'

router.use('/', routerProduct)
router.use('/', routerHello)

export default router