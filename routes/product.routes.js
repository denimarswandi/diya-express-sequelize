import express from 'express';

import {
    getProducts,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct
} from '../controllers/product.controller.js'

const routerProduct = express.Router();

routerProduct.get('/product', getProducts);
routerProduct.get('/product/:id', getProductById);
routerProduct.post('/product', createProduct),
routerProduct.put('/product/:id', updateProduct)
routerProduct.delete('/product/:id', deleteProduct)

export default routerProduct;