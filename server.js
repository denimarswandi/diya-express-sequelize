import express from 'express';
import cors from 'cors';
import db from './config/database.js';
import router from './routes/index.js';

const app = express();
app.use(express.json())
app.use(cors())

try{
    await db.authenticate();
    console.log('db connected')
}catch(err){
    console.error('upps... db not connected')
}

app.use('/',router);

app.listen(5000, ()=>{
    console.log('server run port 5000')
})